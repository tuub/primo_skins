all: deu eng
deu: deu/branding_skin/css/mashup_new.css
	cd deu; zip skin.zip branding_skin/css/mashup_new.css
eng: eng/branding_skin/css/mashup_new.css
	cd eng; zip skin.zip branding_skin/css/mashup_new.css
deu/branding_skin/css/mashup_new.css: deu.less mashup_new.css
	lessc --clean-css="--s0 -b" deu.less deu/branding_skin/css/mashup_new.css
eng/branding_skin/css/mashup_new.css: eng.less mashup_new.css
	lessc --clean-css="--s0 -b" eng.less eng/branding_skin/css/mashup_new.css

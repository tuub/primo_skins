# primo_skins

## install and usage

as root (`sudo su -`)
```
dnf install nodejs
npm install -g less
npm install -g less-plugin-clean-css
```

as standard user:
```
cd ~/devel
git clone git@gitlab.com:tuub/primo_skins.git
cd primo_skins
make
```
